import { Injectable } from '@angular/core';
import { AngularFireDatabase, FirebaseListObservable } from 'angularfire2/database';

@Injectable()
export class NewsService {

	constructor(public db: AngularFireDatabase) {}
    
    getNews(): FirebaseListObservable<any[]> {
		return this.db.list('news');
	};

}