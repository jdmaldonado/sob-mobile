import { Injectable } from '@angular/core';
import { AngularFireDatabase, FirebaseListObservable, FirebaseObjectObservable } from 'angularfire2/database';

import 'rxjs/add/operator/map';


@Injectable()
export class OfferService {

	offersListObservable: FirebaseListObservable<any[]>

    constructor(public db: AngularFireDatabase) {}
    
    getOffersByUserId( userId ): FirebaseListObservable<any[]> {
		return this.offersListObservable = this.db.list('offers',
		{
			query: { 
				orderByChild: 'userId',
				equalTo: userId
			} 
		})
		.map(items => items.sort((a, b) => b.createdDate - a.createdDate)) as FirebaseListObservable<any[]>;
	};

	getNewOfferId() {
		return this.offersListObservable.push({}).key;
	}

	updateOffer( offerId: string, offer: any ) {
		return this.offersListObservable.update(offerId, offer);
	};

	getOfferById(offerId: string): FirebaseObjectObservable<any> {
		return this.db.object(`offers/${offerId}`);
	}

	removeOffer(offerId: string): firebase.Promise<void> {
		return this.offersListObservable.remove(offerId);
	}

}