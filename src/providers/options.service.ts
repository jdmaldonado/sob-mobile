import { Injectable } from '@angular/core';

@Injectable()
export class OptionsService {

    constructor() { }

    getStratumOptions(): any[] {
        return [
            { value: '1', text: '1' },
            { value: '2', text: '2' },
            { value: '3', text: '3' },
            { value: '4', text: '4' },
            { value: '5', text: '5' },
            { value: '6', text: '6' },
            { value: 'country', text: 'COUNTRY_STRATUM' }
        ]
    };

    getOfferTypeOptions(): any[] {
        return [
            { value: 'apartment', text: 'APARTMENT' },
            { value: 'house', text: 'HOUSE' },
            { value: 'studio_apartment', text: 'STUDIO_APARTMENT' },
            { value: 'room', text: 'ROOM' },
            { value: 'farm', text: 'FARM' },
            { value: 'country_house', text: 'COUNTRY_HOUSE' },
            { value: 'cottage', text: 'COTTAGE' },
            { value: 'ground', text: 'GROUND' },
            { value: 'office', text: 'OFFICE' },
            { value: 'shop', text: 'SHOP' },
            { value: 'cellar', text: 'CELLAR' },
            { value: 'building', text: 'BUILDING' }
        ]
    };

    getBusinessTypeOptions(): any[] {
        return [
            { value: 'sale', text: 'BUSINESS_TYPE.SALE' },
            { value: 'lease', text: 'BUSINESS_TYPE.LEASE' },
            { value: 'vacational', text: 'BUSINESS_TYPE.VACATIONAL' },
            { value: 'exchange', text: 'BUSINESS_TYPE.EXCHANGE' },
            { value: 'sale-exchange', text: 'BUSINESS_TYPE.SALE_EXCHANGE' },
            { value: 'lease-exchange', text: 'BUSINESS_TYPE.LEASE_EXCHANGE' }
        ]
    }

    getAntiquityOptions(): any[] {
        return [
            // { value: 'plans', text: 'ANTIQUITY.IN_PLANS' },
            // { value: 'construction', text: 'ANTIQUITY.UNDER_CONSTRUCTION' },
            { value: 'release', text: 'ANTIQUITY.RELEASE' },
            { value: '3_years', text: 'ANTIQUITY.3_YEARS' },
            { value: '6_years', text: 'ANTIQUITY.6_YEARS' },
            { value: '10_years', text: 'ANTIQUITY.10_YEARS' },
            { value: '15_years', text: 'ANTIQUITY.15_YEARS' },
            { value: '20_years', text: 'ANTIQUITY.20_YEARS' },
            { value: 'more_20_years', text: 'ANTIQUITY.MORE_20_YEARS' }
        ]
    }
}