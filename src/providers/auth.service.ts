import { Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import firebase from 'firebase';


@Injectable()
export class AuthService {

    constructor(public afAuth: AngularFireAuth) {
    }

    createAuthUser(_email: string, _password: string): firebase.Promise<any> {
        return this.afAuth.auth.createUserWithEmailAndPassword(_email, _password);
    };

    loginWithPassword(_email: string, _password: string): firebase.Promise<any> {
        return this.afAuth.auth.signInWithEmailAndPassword(_email, _password);
    };

    resetPassword(email: string): firebase.Promise<any> {
        return firebase.auth().sendPasswordResetEmail(email);
    };

    logout(): void {
        this.afAuth.auth.signOut();
    };

}