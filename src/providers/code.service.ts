import { Injectable } from '@angular/core';
import { UserService } from './user.service';

@Injectable()
// This is the service to create a UNIQUE code to users
export class CodeService {

    constructor(private userService: UserService) { }

    async assignCode() {
        let newCode;
        let isInUse = true;
        do {
            newCode = this.generateNewCode();
            isInUse = await this.isCodeInUse(newCode);
        }
        while (isInUse);
        return newCode;
    }

    generateNewCode(): number {
        const min = 100000;
        const max = 999999;
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    private isCodeInUse(code: number): Promise<boolean> {
        return new Promise((resolve, reject) => {
            this.userService.getUserByUniqueCode(code)
                .subscribe(users => {
                    resolve((users && users.length > 0));
                });
        });

    }
}