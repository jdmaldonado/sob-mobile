import { Injectable } from '@angular/core';

import 'rxjs/add/operator/map';

declare var google: any;

@Injectable()
export class GoogleMapsService {
  private defaultPosition: any = {
    lat: 4.570868,
    lng: -74.297333
  };

  private mapMarkers = [];

  constructor() { }

  private convertPosition(position){
    let lat = position.lat || position.latitude || this.defaultPosition.lat ;
    let lng = position.lng || position.longitude || this.defaultPosition.lng;
    return new google.maps.LatLng(lat, lng);
  } 

  loadMap(mapElement, inputElement): string {

    let mapOptions = {
      center: this.convertPosition(this.defaultPosition),
      zoom: 5,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    }

    let map = new google.maps.Map(mapElement.nativeElement, mapOptions);

    if(inputElement) {
      let searchBox = new google.maps.places.SearchBox(inputElement);
      map.controls[google.maps.ControlPosition.LEFT_BOTTOM].push(inputElement);

      searchBox.addListener('places_changed', function() {
          let places = searchBox.getPlaces();
          if(places && places.length > 0) {
            map.panTo(places[0].geometry.location);
            map.setZoom(15);
          }
      });
    }
    
    return map;
  }

  addCircle(map, options: any): any {

    let position = (options && options.position)? this.convertPosition(options.position) : map.getCenter();

    let circle = new google.maps.Circle({
      strokeColor: '#9c27b0',
      strokeOpacity: 0.8,
      strokeWeight: 2,
      fillColor: '#9c27b0',
      fillOpacity: 0.35,
      map: map,
      center: position,
      radius: 500,
      draggable: true
    });

    this.mapMarkers.push(circle);

    return circle;
  }

  addInfoWindow(map, marker, content): void {

    let infoWindow = new google.maps.InfoWindow({
      content: content
    });

    google.maps.event.addListener(marker, 'click', () => {
      infoWindow.open(map, marker);
    });

  }

  addMarker(map, options): void {

    let markerPosition = this.convertPosition(options.position) || map.getCenter();
    let draggable = options.draggable ? true : false;

    let marker = new google.maps.Marker({
      map: map,
      animation: google.maps.Animation.DROP,
      position: markerPosition,
      draggable: draggable
    });

    if (options && options.content) {
      this.addInfoWindow(map, marker, options.content);
    }

  }

  centerMap(map, position: any): void {
    let newPosition = this.convertPosition(position);
    map.panTo(newPosition);
    map.setZoom(15)
  }

  clearMap() {
    for (var i = 0; i < this.mapMarkers.length; i++) {
      this.mapMarkers[i].setMap(null);
    }
  }
  
}
