import { Injectable } from '@angular/core';
import * as firebase from 'firebase';
import { UUID } from 'angular2-uuid';

@Injectable()
export class StorageService {

    constructor() { }

    uploadFile(file: Blob, filePath: string, type: string): Promise<any> {

        let promise = new Promise((resolve, reject) => {
            if (!file) resolve(null);

            let storageRef = firebase.storage().ref();
            let fileName = UUID.UUID();

            let uploadTask = storageRef.child(`${filePath}/${fileName}.${type}`).put(file);

            uploadTask.on('state_changed', {
                next: function () { },
                error: function (err) {
                    reject(err);
                },
                complete: function () {
                    resolve(uploadTask.snapshot.downloadURL);
                }
            });
        });

        return promise;
    }

    uploadMultipleFiles(files: Array<Blob>, filePath: string, type: string): Promise<any> {

        let promises = [];
        files.forEach(fileBlob => {
            promises.push(this.uploadFile(fileBlob, filePath, type));
        });

        return Promise.all(promises);
    }

    removeFile(fileURL): firebase.Promise<any> {
        
        let fileRef = firebase.storage().refFromURL(fileURL);
        return fileRef.delete();
    }

}
