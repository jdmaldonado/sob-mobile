import { Injectable } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';

@Injectable()
export class UserService {

    userId: string

    constructor(private db: AngularFireDatabase) { }

    getLoggedUser(): string {
        return this.userId;
    };

    setLoggedUser(auth): void {
        this.userId = auth.uid;
    };

    // Transactions
    createUser(userId: string, userInfo: any) {
        userInfo.createdDate = new Date().getTime();
        const currentUser = this.db.object('/users/' + userId);
        return currentUser.set(userInfo);
    };

    updateUser( userId: string, data: any ) {
        const userRef = this.db.object(`/users/${userId}`)
		return userRef.update(data);
	};

    getUserById(userId) {
        return this.db.object('/users/' + userId);
    };

    getUserByIdentification(identification: string) {
        return this.db.list('/users', {
            query: {
                equalTo: identification,
                orderByChild: 'identification'
            }
        });
    };

    getUserByUniqueCode(code: number) {
        return this.db.list('/users', {
            query: {
                equalTo: code,
                orderByChild: 'code'
            }
        });
    };
}