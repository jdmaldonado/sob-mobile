import { Injectable } from '@angular/core';
import { ImagePicker } from '@ionic-native/image-picker';
import { Base64 } from '@ionic-native/base64';

import { StorageService } from './storage.service';

declare function unescape(s:string): string;


@Injectable()
export class ImagesService {

  private options: any;

  constructor(
    private base64: Base64,
    private imagePicker: ImagePicker,
    private storageService: StorageService) {
    this.options = {
      maximumImagesCount: 10,
      width: 800,
      height: 600
    }
  }

  // takePhoto() {
  //   this.options.sourceType = this.camera.PictureSourceType.CAMERA;
  //   return this.camera.getPicture(this.options);
  // }

  // uploadFromGallery() {
  //   this.options.sourceType = this.camera.PictureSourceType.PHOTOLIBRARY;
  //   return this.camera.getPicture(this.options);
  // }

  checkPermissions() {
    return new Promise((resolve, reject) => {
      this.imagePicker.hasReadPermission()
        .then((result) => {
          if (result) {
            resolve(true);
          }
          else {
            this.imagePicker.requestReadPermission()
              .then((result) => {
                resolve(result);
              }, (err) => { reject(err) });
          }
        }, (err) => { reject(err) });
    });
  }

  loadImagesFromGallery(maxCount?: number): Promise<any> {
    this.options.maximumImagesCount = maxCount || 10;
    return this.imagePicker.getPictures(this.options);
  }

  base64toBlob(dataURI): Blob {
    var byteString;
    if (dataURI.split(',')[0].indexOf('base64') >= 0)
        byteString = atob(dataURI.split(',')[1]);
    else
        byteString = unescape(dataURI.split(',')[1]);

    // separate out the mime component
    var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];

    // write the bytes of the string to a typed array
    var ia = new Uint8Array(byteString.length);
    for (var i = 0; i < byteString.length; i++) {
        ia[i] = byteString.charCodeAt(i);
    }

    return new Blob([ia], {type:mimeString});
  }

  getBase64Images(results) {

    let promises = [];

    for(let i = 0 ; i < results.length ; i ++ ){
      promises.push(this.base64.encodeFile(results[i]));
    }
    
    return Promise.all(promises);
  }

  saveOfferImages(images64Array: Array<string>, offerId: string) {
    let blobArray = [];
    let path = `offers/${offerId}/images`;
    let type = 'jpg';

    images64Array.forEach(image => {
      let blobFile = this.base64toBlob(image);
      blobArray.push(blobFile);
    });

    return this.storageService.uploadMultipleFiles(blobArray, path, type);
  }

  removeOfferImages(offer) {
    if(offer.images) {
      let promises = [];
      for(let i = 0 ; i < offer.images.length ; i++) {
        promises.push( this.storageService.removeFile(offer.images[i]) );
      }

      return Promise.all(promises);
    }
    else{
      Promise.resolve();
    }
  }

}
