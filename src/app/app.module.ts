import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule, Http } from '@angular/http';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';
import { Base64 } from '@ionic-native/base64';
import { InAppBrowser } from '@ionic-native/in-app-browser';

import { MyApp } from './app.component';

import { EditProfilePage } from '../pages/edit-profile/edit-profile';
import { FormErrorComponent } from '../pages/form-error/form-error';
import { ImagesGalleryPage } from '../pages/images-gallery/images-gallery';
import { OfferForm } from '../pages/offer-form/offer-form';
import { OffersList } from '../pages/offers-list/offers-list';
import { LoginPage } from '../pages/login/login';
import { MapPage } from '../pages/map/map';
import { NewsPage } from '../pages/news/news';
import { NoAuthPage } from '../pages/no-auth/no-auth';
import { ResetPasswordPage } from '../pages/reset-password/reset-password';
import { SignupPage } from '../pages/signup/signup';
import { SettingsPage } from '../pages/settings/settings';
import { TabsPage } from '../pages/tabs/tabs';
import { TutorialPage } from '../pages/tutorial/tutorial';
import { UniqueCodePage } from '../pages/unique-code/unique-code';
import { WelcomePage } from '../pages/welcome/welcome';

import { AuthService } from '../providers/auth.service';
import { CodeService } from '../providers/code.service';
import { OfferService } from '../providers/offer.service';
import { UserService } from '../providers/user.service';
import { UtilsService } from '../providers/utils.service';
import { ImagesService } from '../providers/images.service';
import { StorageService } from '../providers/storage.service';
import { WindowService } from '../providers/window.service';
import { OptionsService } from '../providers/options.service';
import { GoogleMapsService } from '../providers/google-maps.service';
import { NewsService } from '../providers/news.service';

import { SanitizerPipe } from '../pipes/sanitizer.pipe';

import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { ImagePicker } from '@ionic-native/image-picker';
import { SocialSharing } from '@ionic-native/social-sharing';

import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';

// The translate loader needs to know where to load i18n files
// in Ionic's static asset pipeline.
export function HttpLoaderFactory(http: Http) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}


/**
 * The Pages array lists all of the pages we want to use in our app.
 * We then take these pages and inject them into our NgModule so Angular
 * can find them. As you add and remove pages, make sure to keep this list up to date.
 */
let pages = [
  MyApp,
  OfferForm,
  OffersList,
  LoginPage,
  EditProfilePage,
  FormErrorComponent,
  ImagesGalleryPage,
  MapPage,
  NewsPage,
  NoAuthPage,
  ResetPasswordPage,
  SignupPage,
  SettingsPage,
  TabsPage,
  TutorialPage,
  UniqueCodePage,
  WelcomePage
];

export function declarations() {
  return [
    MyApp,
    OfferForm,
    OffersList,
    LoginPage,
    EditProfilePage,
    FormErrorComponent,
    ImagesGalleryPage,
    MapPage,
    NewsPage,
    NoAuthPage,
    ResetPasswordPage,
    SignupPage,
    SettingsPage,
    TabsPage,
    TutorialPage,
    UniqueCodePage,
    WelcomePage,
    SanitizerPipe
  ];
}

export function entryComponents() {
  return pages;
}

export function firebaseConfig() {
  return  {
    apiKey: "AIzaSyDi76xZfev-pz863P4-5xvVCZioPWU7t_s",
    authDomain: "socialbusiness-85a80.firebaseapp.com",
    databaseURL: "https://socialbusiness-85a80.firebaseio.com",
    storageBucket: "socialbusiness-85a80.appspot.com",
    messagingSenderId: "130969567053"
  };
  // return  {
  //   apiKey: "AIzaSyDkvyyMCGWH3Uesd_pmkKYl_3M-DsLYJN8",
  //   authDomain: "social-business-dev.firebaseapp.com",
  //   databaseURL: "https://social-business-dev.firebaseio.com",
  //   projectId: "social-business-dev",
  //   storageBucket: "social-business-dev.appspot.com",
  //   messagingSenderId: "859610282662"
  // };
}

export function providers() {
  return [
    AuthService,
    CodeService,
    ImagesService,
    OfferService,
    StorageService,
    UserService,
    UtilsService,
    SplashScreen,
    StatusBar,
    WindowService,
    OptionsService,
    GoogleMapsService,
    NewsService,
    Geolocation,
    Base64,
    InAppBrowser,
    ImagePicker,
    SocialSharing,
    { provide: ErrorHandler, useClass: IonicErrorHandler }
  ];
}

@NgModule({
  declarations: declarations(),
  imports: [
    BrowserModule,
    HttpModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [Http]
      }
    }),
    AngularFireModule.initializeApp(firebaseConfig()),
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: entryComponents(),
  providers: providers()
})
export class AppModule { }
