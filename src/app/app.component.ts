import { Component, ViewChild } from '@angular/core';
import { Platform, Nav, Config } from 'ionic-angular';
import { AngularFireAuth } from 'angularfire2/auth';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { TranslateService } from '@ngx-translate/core'

import { FirstRunPage } from '../pages/pages';
import { MainPage } from '../pages/pages';
import { WelcomePage } from '../pages/welcome/welcome';

import { UserService } from '../providers/user.service';
import { UtilsService } from '../providers/utils.service';
import { WindowService } from '../providers/window.service';

@Component({
  template: `<ion-nav #content [root]="rootPage"></ion-nav>`
})
export class MyApp {
  rootPage: any = FirstRunPage;

  @ViewChild(Nav) nav: Nav;

  constructor(
    private afAuth: AngularFireAuth,
    private config: Config,
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private translate: TranslateService,
    private userService: UserService,
    private utils: UtilsService,
    private windowService: WindowService) {

    this.initTranslate();

    afAuth.authState.subscribe(auth => {
      if (auth) {
        userService.setLoggedUser(auth);
        this.rootPage = MainPage;
      } else {
        this.rootPage = this.getRootPage();
      }
    });

    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });
  }

  initTranslate() {
    // Set the default language for translation strings, and the current language.
    this.translate.setDefaultLang('es');

    this.translate.get(['BACK_BUTTON_TEXT']).subscribe(values => {
      this.config.set('ios', 'backButtonText', values.BACK_BUTTON_TEXT);
    });
  }

  private getRootPage() {
    const notFirstVisit = this.windowService.window.localStorage.getItem('notFirstVisit');
    const skipLogin = this.windowService.window.localStorage.getItem('skipLogin');

    if (notFirstVisit) {
      if(skipLogin) {
        return MainPage;
      }
      return WelcomePage;
    }
    this.setNotFirstVisit();
    return FirstRunPage;
  }

  private setNotFirstVisit(): void {
    this.windowService.window.localStorage.setItem('notFirstVisit', 'true');
  }
}
