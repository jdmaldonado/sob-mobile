import { Component } from '@angular/core';
import { MenuController, NavController } from 'ionic-angular';

import { WelcomePage } from '../welcome/welcome';

import { TranslateService } from '@ngx-translate/core';


export interface Slide {
  title: string;
  description: string;
  image: string;
}

@Component({
  selector: 'page-tutorial',
  templateUrl: 'tutorial.html'
})
export class TutorialPage {

  slides: Slide[];
  showSkip = true;

  constructor(
    private navCtrl: NavController,
    private menu: MenuController,
    private translate: TranslateService) {
      this.setTranslatedTexts();
  }

  startApp() {
    this.navCtrl.setRoot(WelcomePage, {}, {
      animate: true,
      direction: 'forward'
    });
  }

  setTranslatedTexts(): void  {
    this.translate.get(["SLIDE1_TITLE",
      "SLIDE1_DESCRIPTION",
      "SLIDE2_TITLE",
      "SLIDE2_DESCRIPTION",
      "SLIDE3_TITLE",
      "SLIDE3_DESCRIPTION",
    ]).subscribe(
      (values) => {
        this.slides = [
          {
            title: values.SLIDE1_TITLE,
            description: values.SLIDE1_DESCRIPTION,
            image: 'assets/img/logo/logo.png',
          },
          {
            title: values.SLIDE2_TITLE,
            description: values.SLIDE2_DESCRIPTION,
            image: 'assets/img/users.png',
          },
          {
            title: values.SLIDE3_TITLE,
            description: values.SLIDE3_DESCRIPTION,
            image: 'assets/img/briefcase.png',
          }
        ];
      });
  }

  onSlideChangeStart(slider) {
    this.showSkip = !slider.isEnd;
  }

  ionViewDidEnter() {
    // the root left menu should be disabled on the tutorial page
    this.menu.enable(false);
  }

  ionViewWillLeave() {
    // enable the root left menu when leaving the tutorial page
    this.menu.enable(true);
  }

}
