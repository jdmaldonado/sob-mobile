import { Component } from '@angular/core';
import { NavController, ModalController } from 'ionic-angular';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';

import { MainPage } from '../../pages/pages';
import { UniqueCodePage } from '../../pages/unique-code/unique-code';

import { AuthService } from '../../providers/auth.service';
import { UserService } from '../../providers/user.service';
import { UtilsService } from '../../providers/utils.service';
import { CodeService } from '../../providers/code.service';

import { TranslateService } from '@ngx-translate/core';


@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html'
})
export class SignupPage {

  private userId: string;
  private userCode: number;
  private accountForm: FormGroup
  private successSignUp: string;
  private hasRealState: boolean = false;

  constructor(
    private authService: AuthService,
    private codeService: CodeService,
    private formBuilder: FormBuilder,
    private modalCtrl: ModalController,
    private navCtrl: NavController,
    private translateService: TranslateService,
    private utils: UtilsService,
    private userService: UserService) {

    this.initForm();

    this.translateService.get('SUCCESS_USER_CREATED')
      .subscribe((value) => {
        this.successSignUp = value;
      });
  }

  initForm(): void {
    this.accountForm = this.formBuilder.group({
      email: ['', Validators.required],
      password: ['', Validators.required],
      // identification: ['', Validators.required],
      name: ['', Validators.required],
      lastName: ['', Validators.required],
      phone: [null],
      cellPhone: ['', Validators.required],
      city: ['', Validators.required],
      realState: this.formBuilder.group({
        name: [null],
        nit: [null],
        phone: [null]
      })
    });
  }

  doSignup() {
    if (this.hasRealState && !this.checkRealStateInfo()) {
      this.utils.showMessage('La opción inmobiliaria se encuentra activada, debe diligenciar todos los datos de la inmobiliaria', 5000);
      return;
    }

    if(!this.hasRealState) {
      this.clearRealState();
    }

    this.utils.showLoading();

    this.authService.createAuthUser(this.accountForm.value.email, this.accountForm.value.password)
      // .then(userAlreadyExist => {
      //   if (userAlreadyExist) {
      //     return Promise.reject({ message: 'Ya existe un usuario con este número de identificación' });
      //   }
      //   else {
      //     return 
      //   }
      // })
      .then(authData => {
        delete this.accountForm.value.password;
        this.userId = authData.uid;
        return this.userService.createUser(this.userId, this.accountForm.value);
      })
      .then(_ => {
        return this.codeService.assignCode();
      })
      .then(userCode => {
        this.userCode = userCode;
        return this.userService.updateUser(this.userId, { code: userCode });
      })
      .then(_ => {
        this.initForm();
        this.showNewCodeModal(this.userCode);
        this.utils.showMessage(this.successSignUp, 3000);
      })
      .catch(error => {
        this.utils.showMessage(error.message, 5000);
        this.utils.hideLoading();
      });
  }

  clearRealState(): void {
    this.accountForm.controls['realState'].patchValue({
      name: null,
      nit: null,
      phone: null
    });
  }

  checkRealStateInfo(): boolean {
    let realStateProperties = this.accountForm.value.realState;
    for (var member in realStateProperties) {
      if (realStateProperties[member] === null)
        return false;
    }
    return true;
  }

  validateIdentification(): Promise<any> {
    return new Promise((resolve, reject) => {
      this.userService.getUserByIdentification(this.accountForm.value.identification)
        .subscribe(users => {
          resolve(users && users.length > 0);
        });
    });
  }

  showNewCodeModal(userCode: number): void {
    let modal = this.modalCtrl.create(UniqueCodePage, { uniqueCode: userCode });

    modal.onDidDismiss(data => {
      this.navCtrl.setRoot(MainPage);
    });
    modal.present();
  }
}
