import { Component } from '@angular/core';
import { InAppBrowser } from '@ionic-native/in-app-browser';

import { NewsService } from '../../providers/news.service';

@Component({
  selector: 'page-news',
  templateUrl: 'news.html',
})
export class NewsPage {
  
  public news;

  constructor(private newsService: NewsService, private browser: InAppBrowser) {
    this.news = this.newsService.getNews();
  }

  openLink(url: string): void {
    this.browser.create(url);
  }

}
