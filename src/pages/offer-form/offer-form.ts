import { Component, OnInit } from '@angular/core';
import { NavController, ActionSheetController, NavParams, ModalController } from 'ionic-angular';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';

import { MapPage } from '../../pages/map/map';
import { ImagesGalleryPage } from '../../pages/images-gallery/images-gallery';

import { OfferService } from '../../providers/offer.service';
import { UtilsService } from '../../providers/utils.service';
import { ImagesService } from '../../providers/images.service';
import { UserService } from '../../providers/user.service';
import { StorageService } from '../../providers/storage.service';
import { OptionsService } from '../../providers/options.service';

declare var cordova: any

@Component({
  templateUrl: 'offer-form.html',
  selector: 'offer-form'
})


export class OfferForm implements OnInit {

  private userId: string;
  private offerId: string;
  private images: Array<string>;
  private newImages: Array<string>;
  public offerForm: FormGroup
  public showComponents: Boolean = false;
  //Options
  private stratumOptions: any[];
  private typeOptions: any[];
  private businessTypeOptions: any[];
  private antiquityOptions: any[];

  constructor(
    private utils: UtilsService,
    private navParams: NavParams,
    private navCtrl: NavController,
    private formBuilder: FormBuilder,
    private userService: UserService,
    private offerService: OfferService,
    private modalCtrl: ModalController,
    private imagesService: ImagesService,
    private storageService: StorageService,
    private optionsService: OptionsService,
    private actionSheetCtrl: ActionSheetController) {

    this.initOptions();

    this.offerForm = this.formBuilder.group({
      type: ['', Validators.required],
      businessType: ['', Validators.required],
      name: ['', Validators.required],
      neighborhood: ['', Validators.required],
      department: [''],
      city: ['', Validators.required],
      address: [''],
      price: [null, Validators.required],
      administration: [null],
      area: [null],
      description: [null, Validators.required],
      thirdParty: [false, Validators.required],
      components: this.formBuilder.group({
        rooms: [null],
        bathrooms: [null],
        parkings: [null],
        socialStratum: [null],
        antiquity: [null]
      }),
      location: this.formBuilder.group({
        lat: [null],
        lng: [null],
      }),
      images: [[]],
      createdDate: [null],
      userId: [null]
    });

    if (this.navParams.data.offerId) {
      this.offerId = this.navParams.data.offerId;
      this.fillFields(this.offerId);
    }
    else {
      this.offerId = this.offerService.getNewOfferId();
    }
  }

  ngOnInit() {
    this.userId = this.userService.getLoggedUser();
  }

  upsertOffer(): void {
    this.utils.showLoading('Guardando Oferta...');
    this.setUserId();
    this.setCreatedDate();
    
    if(this.offerForm.value.images.length <= 0 && !this.newImages) {
      this.utils.showMessage(`Debe cargar al menos una imágen`, 5000);
      this.utils.hideLoading();
      return;
    }

    this.offerService.updateOffer(this.offerId, this.offerForm.value)
    .then(() => {
      if(this.newImages) {
        return this.imagesService.saveOfferImages(this.newImages, this.offerId);
      }
      else {
        Promise.resolve();
      }
    })
    .then(imageUrls => {
      if (imageUrls) {
        let images = this.mergeImages(imageUrls);
        return this.offerService.updateOffer(this.offerId, { images: images });
      }
      else {
        return Promise.resolve();
      }
    })
    .then(() => {
      this.utils.hideLoading();
      this.utils.showMessage('Oferta guardada exitosamente', 3000),
        this.navCtrl.pop();
    })
    .catch(err => this.utils.showMessage(`Error al agregar la oferta, ${err.message}`, 5000));
  }
  
  private manageImages(): void {
    let imagesModal = this.modalCtrl.create(ImagesGalleryPage, 
      { 
        offer: this.offerForm, 
        offerId: this.offerId,
        galleryImages: this.newImages 
      });

    imagesModal.onDidDismiss(newImages => {
      if (newImages) {
       this.newImages = newImages;
      }
    });

    imagesModal.present();
  }

  private fillFields(offerId: string): void {

    // Get offer Info
    this.offerService.getOfferById(offerId)
      .subscribe(
      (offer) => {
        this.offerForm.patchValue(offer);
        this.images = offer.images;
      },
      (err) => this.utils.showMessage(err.message, 5000),
      () => console.log(`terminated fillFields`))
  }

  private initOptions(): void {
    this.stratumOptions = this.optionsService.getStratumOptions();
    this.typeOptions = this.optionsService.getOfferTypeOptions();
    this.businessTypeOptions = this.optionsService.getBusinessTypeOptions();
    this.antiquityOptions = this.optionsService.getAntiquityOptions();
  }

  private openMap() {
    let mapModal = this.modalCtrl.create(MapPage, this.offerForm.controls['location']);
    mapModal.onDidDismiss(data => {
      if (data) {
        if (data === 'remove') {
          this.offerForm.controls['location'].patchValue({
            lat: null,
            lng: null
          });
        }
        else {
          this.offerForm.controls['location'].patchValue(data);
        }
      }
    });
    mapModal.present();
  }

  private mergeImages(newImages: Array<string>): Array<string> {
    this.images = this.images || [];
    newImages.forEach(newImage => {
      this.images.push(newImage);
    });

    return this.images;
  }

  private setCreatedDate(): void {
    let today = new Date();
    this.offerForm.controls['createdDate'].patchValue(today.getTime());
  }

  private setUserId(): void {
    this.offerForm.controls['userId'].patchValue(this.userId);
  }

}

