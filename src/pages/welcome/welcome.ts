import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import { LoginPage } from '../login/login';
import { MainPage } from '../../pages/pages';
import { SignupPage } from '../signup/signup';

import { UtilsService } from '../../providers/utils.service';
import { WindowService } from '../../providers/window.service';

@Component({
  selector: 'page-welcome',
  templateUrl: 'welcome.html'
})
export class WelcomePage {

  constructor(
    private navCtrl: NavController,
    private utils: UtilsService,
    private windowService: WindowService) {}

  login() {
    this.navCtrl.push(LoginPage);
  }

  signup() {
    this.navCtrl.push(SignupPage);
  }

  skip() {
    this.windowService.window.localStorage.setItem('skipLogin', 'true');
    this.navCtrl.setRoot(MainPage);
  }
}
