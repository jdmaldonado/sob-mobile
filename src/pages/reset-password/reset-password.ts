import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import { UtilsService } from '../../providers/utils.service';
import { AuthService } from '../../providers/auth.service';

@Component({
  selector: 'page-reset-password',
  templateUrl: 'reset-password.html',
})
export class ResetPasswordPage {

  private email: string;
  private showConfirm: boolean = false;

  constructor(
    public navCtrl: NavController,
    public utils: UtilsService,
    public authService: AuthService) {}

  resetPassword () {
    this.showConfirm = false;
    this.utils.showLoading();

    this.authService.resetPassword(this.email)
    .then(_ => {
      this.utils.hideLoading();
      this.showConfirm = true;
    })
    .catch(err => {
      this.utils.hideLoading();
      this.utils.showMessage('Este correo no se encuentra registrado en nuestro sistemas, debes registrarte', 5000);
    })
  }
  
}
