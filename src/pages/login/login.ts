import { Component } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';

import { MainPage } from '../../pages/pages';
import { ResetPasswordPage } from '../../pages/reset-password/reset-password';
import { SignupPage } from '../../pages/signup/signup';

import { UtilsService } from '../../providers/utils.service';
import { AuthService } from '../../providers/auth.service';

import { TranslateService } from '@ngx-translate/core';


@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {

  // Our translated text strings
  private loginErrorString: string;
  private account: { email: string, password: string} = {
    email: '',
    password: ''
  };

  private isModal: boolean;

  constructor(
    private authService: AuthService,
    private navCtrl: NavController,
    private params: NavParams,
    private utils: UtilsService,
    private translateService: TranslateService,
    private viewCtrl: ViewController) {

    this.isModal = this.params.get('isModal') || false;

    this.translateService.get('LOGIN_ERROR')
    .subscribe((value) => {
      this.loginErrorString = value;
    });
  }
  
  login() {
      this.utils.showLoading('Cargando ...');

      this.authService.loginWithPassword(this.account.email, this.account.password)
      .then( response => {
        this.navCtrl.setRoot(MainPage);
      })
      .catch( error => {
        console.error(error);
        this.utils.showMessage( this.loginErrorString + error.message, 5000 );
        this.utils.hideLoading();
      });
  }

  resetPassword(): void {
    this.navCtrl.push(ResetPasswordPage);
  }

  dismiss(): void {
    this.viewCtrl.dismiss();
  }

  goToSignUp(): void {
    this.navCtrl.push(SignupPage)
  }
}
