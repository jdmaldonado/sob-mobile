import { Component } from '@angular/core';
import { NavController, NavParams, ActionSheetController, ViewController } from 'ionic-angular';

import { ImagesService } from '../../providers/images.service';
import { StorageService } from '../../providers/storage.service';
import { UtilsService } from '../../providers/utils.service';
import { OfferService } from '../../providers/offer.service';

@Component({
  selector: 'images-gallery',
  templateUrl: 'images-gallery.html'
})
export class ImagesGalleryPage {

  offer: any;
  offerId: string;
  grid: Array<any> = [];
  loadedImages: Array<any> = [];
  galleryImages: Array<any> = [];

  constructor(
    private actionSheetCtrl: ActionSheetController,
    private imagesService: ImagesService,
    private params: NavParams,
    private offerService: OfferService,
    private storageService: StorageService,
    private utils: UtilsService,
    private viewCtrl: ViewController) {

    this.initImagesGrid();
    this.offer = this.params.get('offer');
    this.offerId = this.params.get('offerId');
    let savedImages = this.params.get('galleryImages');

    if (this.offer.controls.images && this.offer.controls.images._value && this.offer.controls.images._value.length > 0) {
      this.loadedImages = this.offer.controls.images._value;
    }

    // Son las imágenes que ya se cargaron desde la librería, simplemente volvió a verificar o a cambiarlas
    if(savedImages){
      this.galleryImages = savedImages;
    }

    this.refreshImages();
  }

  private confirmDismiss(saveData?: boolean): void {
    if (saveData) {
      this.viewCtrl.dismiss(this.galleryImages);
    }
    else {
      this.viewCtrl.dismiss();
    }
  }

  private initImagesGrid(): void {
    this.grid = [];
    for (let i = 0; i < 10; i++) {
      this.grid.push({
        image: null,
        uploaded: false
      });
    };
  }

  private fillGalleryImages(): void {
    let count = 0;
    let images = this.galleryImages;
    for (let i = this.loadedImages.length; i < this.loadedImages.length + images.length; i++) {

      let item = {
        image: images[count],
        uploaded: false
      }
      this.grid[i] = item;
      count++;
    }
  }

  private fillLoadedImages(): void {
    for (let i = 0; i < this.loadedImages.length; i++) {
      let item = {
        image: this.loadedImages[i],
        uploaded: true
      }
      this.grid[i] = item;
    }
  }

  private refreshImages(): void {
    this.initImagesGrid();
    this.fillLoadedImages();
    this.fillGalleryImages();
  };

  private removeFromArray(imageUrl, imagesArray: Array<string>): Promise<any> {
    return new Promise((resolve, reject) => {
      let index = imagesArray.indexOf(imageUrl);
      if (index !== -1) {
        imagesArray.splice(index, 1);
        resolve();
      }
      else {
        reject({ message: 'Image Reference not found' });
      }
    });
  }

  showCancelDialog(): void {

    let actionSheet = this.actionSheetCtrl.create({
      title: '¿ Desea salir ?, se perderá el proceso',
      buttons: [
        {
          icon: 'checkmark',
          text: 'Si, Deseo volver al formulario',
          handler: () => {
            this.confirmDismiss(false);
          }
        },
        {
          icon: 'close',
          text: 'Cancelar',
          role: 'cancel'
        }
      ]
    });

    actionSheet.present();

  }

  uploadFromGallery(): void {

    this.imagesService.checkPermissions()
      .then(hasPermissions => {
        let maxCount = 10 - this.loadedImages.length;
        if (maxCount > 0) {
          return this.imagesService.loadImagesFromGallery(maxCount);
        }
        else {
          return Promise.reject({ message: 'Sólo puede cargar 10 imágenes, debe eliminar alguna para poder continuar' })
        }
      })
      .then(loadedImages => {
        return this.imagesService.getBase64Images(loadedImages)
      })
      .then(base64Array => {
        this.galleryImages = base64Array;
        this.fillGalleryImages();
      })
      .catch(err => { this.utils.showMessage(err.message, 5000); });
  }

  removeImage(image: any): void {
    // Is in firebase
    if (image.uploaded) {
      this.storageService.removeFile(image.image)
        .then(_ => {
          return this.removeFromArray(image.image, this.loadedImages);
        })
        .then(_ => {
          return this.offerService.updateOffer(this.offerId, { images: this.loadedImages })
        })
        .then(_ => {
          this.refreshImages();
          this.utils.showMessage('Imágen removida con éxito', 3000);
        })
        .catch(err => {
          this.utils.showMessage(err.message, 5000);
        })
    }
    else {
      this.removeFromArray(image.image, this.galleryImages)
        .then(_ => {
          this.refreshImages();
          this.utils.showMessage('Imágen removida con éxito', 3000);
        })
        .catch(err => {
          this.utils.showMessage(err.message, 5000);
        })
    }
  }

  showConfirmRemove(image: any): void {

    let actionSheet = this.actionSheetCtrl.create({
      title: '¿ Desea eliminar esta imágen ?',
      buttons: [
        {
          icon: 'checkmark',
          text: 'Si, Eliminar',
          handler: () => {
            this.removeImage(image);
          }
        },
        {
          icon: 'close',
          text: 'Cancelar',
          role: 'cancel'
        }
      ]
    });

    actionSheet.present();

  }
}
