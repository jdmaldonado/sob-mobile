import { Component } from '@angular/core';
import { ModalController } from 'ionic-angular';

import { LoginPage } from '../login/login';

@Component({
  selector: 'no-auth',
  templateUrl: 'no-auth.html',
})
export class NoAuthPage {

  constructor(private modalCtrl: ModalController) {}

  openAuthModal(): void {
    let authModal = this.modalCtrl.create(LoginPage, { isModal: true });

    authModal.present();
  }

}
