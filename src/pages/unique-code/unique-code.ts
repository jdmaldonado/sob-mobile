import { Component } from '@angular/core';
import { NavController, NavParams, ActionSheetController, ViewController } from 'ionic-angular';

@Component({
  selector: 'page-unique-code',
  templateUrl: 'unique-code.html',
})
export class UniqueCodePage {

  private uniqueCode: string;

  constructor(
    private navCtrl: NavController, 
    private params: NavParams,
    private viewCtrl: ViewController) {
    this.uniqueCode = this.params.get('uniqueCode');
  }

  private dismiss(): void {
    this.viewCtrl.dismiss();
  }

}
