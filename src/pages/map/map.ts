import { Component, ViewChild, ElementRef } from '@angular/core';
import { ViewController, ActionSheetController, NavParams } from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';

import { TranslateService } from '@ngx-translate/core';

import { GoogleMapsService } from '../../providers/google-maps.service';

@Component({
  selector: 'map',
  templateUrl: 'map.html'
})
export class MapPage {

  @ViewChild('map') mapElement: ElementRef;

  map: any;
  zone: any;
  coords: any;
  showZone: Boolean = false;


  constructor(
    private actionSheetCtrl: ActionSheetController,
    private googleMapsService: GoogleMapsService,
    private geolocation: Geolocation,
    private params: NavParams,
    private translate: TranslateService,
    private viewCtrl: ViewController) {

    if (params.data.controls.lat.value) {
      this.showZone = true;
      this.coords = {
        lat: params.data.controls.lat.value,
        lng: params.data.controls.lng.value
      }
    }

  }

  ngAfterViewInit() {
    let inputMap = document.getElementById('mapInput');
    this.map = this.googleMapsService.loadMap(this.mapElement, inputMap);
    if (this.showZone) {
      this.googleMapsService.centerMap(this.map, this.coords);
      this.setLocation({ position: this.coords });
    }
    else {
      this.geolocation.getCurrentPosition()
        .then((resp) => {
          this.coords = resp.coords;
          this.googleMapsService.centerMap(this.map, this.coords);
        })
        .catch((error) => {
          console.log(error);
        });
    }
  }

  setLocation(coords) {
    this.zone = this.googleMapsService.addCircle(this.map, coords);
  }

  clearMap() {
    this.zone = null;
    this.googleMapsService.clearMap();
  }

  confirmDismiss(saveData) {
    if (saveData) {
      if (this.zone && this.zone.center) {
        let data = {
          lat: this.zone.center.lat(),
          lng: this.zone.center.lng()
        };
        this.viewCtrl.dismiss(data);
      }
      else {
        this.viewCtrl.dismiss('remove');
      }
    }
    else {
      this.viewCtrl.dismiss();
    }
  }

  showConfirmDialog(): void {

    let actionSheet = this.actionSheetCtrl.create({
      title: '¿ Desea salir del mapa, se perderá el proceso ?',
      buttons: [
        {
          icon: 'checkmark',
          text: 'Si, Deseo volver al formulario',
          handler: () => {
            this.confirmDismiss(true);
          }
        },
        {
          icon: 'close',
          text: 'No, Deseo volver al mapa',
          role: 'cancel'
        }
      ]
    });

    actionSheet.present();

  }
}
