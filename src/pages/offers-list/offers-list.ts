import { Component, OnInit } from '@angular/core';
import { NavController, ActionSheetController } from 'ionic-angular';
import { FirebaseListObservable } from 'angularfire2/database';
import { Observable } from 'rxjs/Rx';
import { AngularFireAuth } from 'angularfire2/auth';
import { SocialSharing } from '@ionic-native/social-sharing';

import { OfferForm } from '../offer-form/offer-form';
import { WelcomePage } from '../welcome/welcome';

import { OfferService } from '../../providers/offer.service';
import { ImagesService } from '../../providers/images.service';
import { UserService } from '../../providers/user.service';
import { UtilsService } from '../../providers/utils.service';

@Component({
  selector: 'offers-list',
  templateUrl: 'offers-list.html'
})

export class OffersList {

  private userId: string;
  private user: Observable<firebase.User>;
  private offers: FirebaseListObservable<any[]>;
  private tempImages: Array<string> = [];

  constructor(
    private actionSheetCtrl: ActionSheetController,
    private afAuth: AngularFireAuth,
    private imagesService: ImagesService,
    private navCtrl: NavController,
    private offerService: OfferService,
    private socialSharing: SocialSharing,
    private userService: UserService,
    private utils: UtilsService) {

    this.utils.showLoading('Cargando...');
    this.user = afAuth.authState;
    this.user.subscribe(auth => {
      if (auth) {
        this.initOffers();
      }
    });
  }

  initOffers(): void {
    this.userId = this.userService.getLoggedUser();
    this.offers = this.offerService.getOffersByUserId(this.userId);
    this.initTempImages();
    this.utils.hideLoading();
  }

  ngAfterViewInit() {
    this.utils.hideLoading();
  }

  goToAddOffer(): void {
    this.navCtrl.push(OfferForm);
  }

  editOffer(offerId: string): void {
    this.navCtrl.push(OfferForm, {
      offerId: offerId
    })
  }

  private initTempImages(): void {
    for (let i = 0; i < 5; i++) {
      this.tempImages.push('./assets/img/logo/logo.png');
    }
  }

  private showRemoveDialog(offer: any): void {

    let actionSheet = this.actionSheetCtrl.create({
      title: `¿ Desea Eliminar la oferta ${offer.name}?, Este proceso no podra deshacerse`,
      buttons: [
        {
          icon: 'checkmark',
          text: 'Si, Eliminar Oferta',
          handler: () => {
            this.confirmRemove(offer);
          }
        },
        {
          icon: 'close',
          text: 'Cancelar',
          role: 'cancel'
        }
      ]
    });

    actionSheet.present();

  }

  private confirmRemove(offer: any): void {
    this.utils.showLoading('Estamos eliminado tu oferta...');

    this.offerService.removeOffer(offer.$key)
      .then(_ => {
        return this.imagesService.removeOfferImages(offer);
      })
      .then(_ => {
        this.utils.hideLoading();
        this.utils.showMessage('Oferta eliminada con éxito', 3000);
      })
      .catch(err => {
        this.utils.hideLoading();
        this.utils.showMessage(err.message, 5000);
      })
  }

  shareOffer(offer): void {
    let url = `https://socialbusiness-85a80.firebaseapp.com/offer/${offer.$key}`

    this.socialSharing.share(offer.name, offer.description, null, url)
      .then(data => {
        console.log(data);
      })
      .catch(err => {
        console.log(err);
      })
  }

}

