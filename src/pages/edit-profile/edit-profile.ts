import { Component } from '@angular/core';
import { NavController, ModalController } from 'ionic-angular';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';

import { UserService } from '../../providers/user.service';
import { UtilsService } from '../../providers/utils.service';

@Component({
  selector: 'page-edit-profile',
  templateUrl: 'edit-profile.html',
})
export class EditProfilePage {

  private hasRealState: boolean;
  private profileForm : FormGroup;
  private userId: string;

  constructor(
    private formBuilder: FormBuilder,
    private userService: UserService,
    private utils: UtilsService,
  
  ) {
    this.utils.showLoading('Cargando...');
    this.userId = this.userService.getLoggedUser();
    this.initForm();
    this.fillForm();
    this.utils.hideLoading();
  }

  private fillForm() {
    this.userService.getUserById(this.userId)
    .subscribe((userInfo) => {
      this.profileForm.patchValue(userInfo);
      if(userInfo.realState){
        this.hasRealState = true;
      }
    },
    (err) => this.utils.showMessage(err.message, 5000));
  }

  private initForm() {
    this.profileForm = this.formBuilder.group({
      email: ['', Validators.required],
      identification: ['', Validators.required],
      name: ['', Validators.required],
      lastName: ['', Validators.required],
      phone: [null],
      cellPhone: ['', Validators.required],
      city: ['', Validators.required],
      realState: this.formBuilder.group({
        name: [null],
        nit: [null],
        phone: [null]
      })
    });
  }

  private checkRealStateInfo(): boolean {
    let realStateProperties = this.profileForm.value.realState;
    for (var member in realStateProperties) {
      if (realStateProperties[member] === null)
        return false;
    }
    return true;
  }

  private clearRealState(): void {
    this.profileForm.controls['realState'].patchValue({
      name: null,
      nit: null,
      phone: null
    });
  }

  upsertProfile() {
    if (this.hasRealState && !this.checkRealStateInfo()) {
      this.utils.showMessage('La opción inmobiliaria se encuentra activada, debe diligenciar todos los datos de la inmobiliaria', 5000);
      return;
    }

    if(!this.hasRealState) {
      this.clearRealState();
    }

    this.utils.showLoading();

    this.userService.updateUser(this.userId, this.profileForm.value )
    .then(_ => {
      this.utils.showMessage('Perfil actualizado con éxito', 3000);
      this.utils.hideLoading();
    })
    .catch(error => {
      this.utils.showMessage(error.message, 5000);
      this.utils.hideLoading();
    });
  }

}
