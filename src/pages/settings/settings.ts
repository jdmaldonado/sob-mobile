import { Observable } from 'rxjs/Rx';
import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import { NavController, NavParams } from 'ionic-angular';
import { FirebaseObjectObservable } from 'angularfire2/database';

import { EditProfilePage } from '../../pages/edit-profile/edit-profile';

import { AuthService } from '../../providers/auth.service';
import { UserService } from '../../providers/user.service';

@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html',
})
export class SettingsPage {

  private userId: string;
  public authUser: Observable<firebase.User>;
  private user: FirebaseObjectObservable<any>;

  constructor(
    public authService: AuthService,
    private afAuth: AngularFireAuth,
    public userService: UserService,
    public navCtrl: NavController,
    public navParams: NavParams) {
      this.authUser = afAuth.authState;
      this.authUser.subscribe(auth => {
        if (auth) {
          this.initSettings();
        }
      });
    }
    
    initSettings(): void {
      this.userId = this.userService.getLoggedUser();
      this.user = this.userService.getUserById(this.userId);
  }

  editProfile() {
    this.navCtrl.push(EditProfilePage)
  }

  logout() {
    this.authService.logout();
  }

}
